var searchData=
[
  ['sample_5ffrom',['sample_from',['../structcic_1_1_settings_1_1_simulator.html#acb05764e1358dcb77d1370c4f652600f',1,'cic::Settings::Simulator']]],
  ['sample_5fstep',['sample_step',['../structcic_1_1_settings_1_1_simulator.html#a7b590e3ce3787e2a40a97928e7c0275f',1,'cic::Settings::Simulator']]],
  ['sample_5fto',['sample_to',['../structcic_1_1_settings_1_1_simulator.html#a94470b7f006bf76b770518479aac9c5c',1,'cic::Settings::Simulator']]],
  ['sampleaveragecount',['sampleAverageCount',['../structcic_1_1_settings_1_1_neural.html#a42d9501a5e2bd9887e5793a9d5fdb717',1,'cic::Settings::Neural']]],
  ['selection_5ftype',['selection_type',['../structcic_1_1_settings_1_1_g_p.html#a569a10679151c5b1b1ef935da373a114',1,'cic::Settings::GP']]],
  ['showandquitoninputpopulation',['showAndQuitOnInputPopulation',['../structcic_1_1_settings_1_1_control.html#af8989fc53194fd76effba94f9f1b7934',1,'cic::Settings::Control']]],
  ['synopsisthreshold',['synopsisThreshold',['../structcic_1_1_settings_1_1_neural.html#a6d2b83460c7829643eaaa6e66e18144b',1,'cic::Settings::Neural']]]
];
